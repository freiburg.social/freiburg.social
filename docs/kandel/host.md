# Firewall

Firewall-Konfiguration machen wir mit UFW.
Grund für die Firewall ist, dass man ansonsten mal aus Versehen einen Port nach außen öffnet. Außerdem verhindert man damit, dass eventuell Schadsoftware einen Port nach außen öffnet.

## Ports

- 25 für SMTP
- 80 / 443 für NGINX
- 555 für SSH Host

Nachfolgende Ports für SSH in die entsprechenden Container:
- 556 für SSH mastodon (noch nicht belegt)
- 557 für SSH mail
- 556 für SSH nC

## Öffnen eines Ports in der Firewall

Eine neue Firewall-Regel baut man so ein:
```
ufw allow 558/tcp comment 'ssh to nc, port 558'
```

Gut ist, wie oben, dabei einen Kommentar einzugeben.

Nach dem Update auf Ubuntu LTS 22.04 war es notwendig den Zugriff
auf das lxd network interface für die Container freizugeben.
Ansonsten hat DHCP nicht funktioniert.

Dafür haben wir folgende Befehle ausgeführt:

```
ufw allow in on lxdbr0
ufw route allow in on lxdbr0
ufw route allow out on lxdbr0
```

## Port Forwarding SMTP (Port 25) & IMAP (Port 993)

Hier am Beispiel von SMTP. IMAP ist äquivalent konfiguriert (993).
Ist manuell konfiguriert in `/etc/ufw/before.rules`

```
*nat
:PREROUTING - [0:0]
-A PREROUTING -i ens3 -d 202.61.243.182 -p tcp --dport 25 -j DNAT --to-destination 10.126.61.16:25
COMMIT
```

und in `/etc/ufw/before6.rules`:

```
*nat
:PREROUTING - [0:0]
-A PREROUTING -i ens3 -d 2a03:4000:52:fb6::1 -p tcp --dport 25 -j DNAT --to-destination [fd42:deea:f0ef:5f09:216:3eff:feb1:1a83]:25
COMMIT
```

Dazu ist ein `ufw allow 25/tcp` notwendig, damit eine Verbindung mit dem Port 25 überhaupt erlaubt wird.

Danach ist außerdem ein `ufw reload` notwendig um die Änderungen zu übernehmen.


# SSL

## Zertifikate mit NGINX

Wir machen die Zertifikate mit `certbot` bei [Let's Encrypt](https://letsencrypt.org/).
Wir haben certbot über das Paket `certbot` aus den Ubuntu-Repositories installiert.

### Neues Zertifikat

Achtung: Folgendes Kommando ändert die entsprechende Konfiguration von nginx.
Es lohnt sich also ein Backup zu machen um hinterher die Unterschiede zu sehen.

```
# cp -r /etc/nginx /etc/nginx.$(date --iso)
# certbot --no-redirect --nginx -d $HOSTNAME
# diff -ur /etc/nginx.$(date --iso) /etc/nginx
```

### Renewal

Renewal läuft über einen systemd-Timer. Service & Time-File kommen ebenfalls aus dem Ubuntu-Paket.

Möchte man die Logs sehen von certbot, kann man das zum Beispiel mit folgendem Kommand:

```
journalctl --pager-end --unit certbot.service
```

Manuell ausführen kann man das mit

```
systemctl start certbot.service
```

`systemctl list-timers` gibt Aufschluss darüber, wann der Timer das letzte Mal gelaufen ist und wann er das nächste Mal ausgeführt werden soll.

## Redirect HTTP->HTTPS

Wir haben die Weiterleitung von HTTP nach HTTPS zentral konfiguriert, sodass man nicht für jede Site die Weiterleitung manuell einrichten muss:

```
# cat /etc/nginx/conf.d/https_redirect.conf
# Configuration from:
# https://linuxize.com/post/redirect-http-to-https-in-nginx/#redirect-all-sites-to-https
server {
    listen 80 default_server;
    # listen [::]:80 default_server;
    server_name _;
    # basic status page for Zabbix plugin
    # requires wrapping redirect into a location
    # bochmann 20221108
    location / {
        return 301 https://$host$request_uri;
    }
    location = /basic_status {
        stub_status on;
        allow 127.0.0.1;
        allow ::1;
        deny all;
    }
    # return 301 https://$host$request_uri;
}
```

Aufgrund von Warnungen im nginx-Log wurde in `/etc/letsencrypt/options-ssl-nginx.conf` der ssl_session_cache - Wert vom Default (10m) abgeändert:
```
ssl_session_cache shared:le_nginx_SSL:20m;
```

## sonstige Host-Settings

- multipathd deaktiviert, in unserem Setup unnötig
```
systemctl stop multipathd && systemctl dsiable multipathd && systemctl mask multipathd
```
- swap-Nutzung leicht verringern
```
# cat /etc/sysctl.d/99-swap.conf
vm.swappiness = 1
vm.vfs_cache_pressure=200
```

## zswap

Zswap verringert den Disk-Swap durch komprimierte Swap-Pages im RAM
https://www.kernel.org/doc/html/latest/admin-guide/mm/zswap.html

Zur Aktivierung werden auf kandel die sysfstools verwendet:

`apt install sysfstools`

```
# cat /etc/sysfs.d/zswap.conf
module/zswap/parameters/compressor = lz4
module/zswap/parameters/zpool = z3fold
module/zswap/parameters/max_pool_percent = 10
module/zswap/parameters/enabled = 1
```

Benötigte Kernelmodule:
```
# cat /etc/modules-load.d/zswap.conf
lz4
z3fold
```
