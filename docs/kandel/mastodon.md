# Mastodon

## Regulare Mastodon Releases einspielen

Wir haben in unserem Repository ein paar Kleinigkeiten gegenüber des Standardcodes geändert.
Unseren Code findet man in diesem [Gitlab Repository](https://gitlab.com/freiburg.social/mastodon)
im Branch `prod`.

Ein Relase zu machen ist deshalb ein kleines bisschen aufwändiger.

### Einmalige Einrichtung

Zuerst einmal muss man sich lokal unser Repository auschecken:
```
git clone git@gitlab.com:freiburg.social/mastodon.git
cd mastodon
```

Zusätzlich zu unserem Repository muss man das original-Repository hinzufügen:
```
git remote add original git@github.com:mastodon/mastodon.git
```

### Eigentliches Release

Einmal kontrollieren, ob wir auf dem richtigen Branch sind mit
```
$ git status
Auf Branch prod
...
```

Ganz oben in der Ausgabe sollte stehen `Auf Branch prod`. Ist das nicht der Fall, muss man in den
Branch wechseln mit `git checkout prod`.

Dann müssen wir unseren lokalen Stand aktualisieren
```
git pull # holt Anderungen von unserem gitlab und aktualisiert den lokale Brach
git fetch --tags original # holt die aktuellen Release Tags von Upstream
```

Bei der Eingabe von `git log` sieht man, auf welchem Upstream-Stand wir laufen:

```
$ git log
commit bc51235dc5e2119d5873266d3d6a46c71086b4f8 (HEAD -> prod, origin/prod)
Author: Bastian Senst <senst@cp-med.com>
Date:   Wed Nov 9 16:10:15 2022 +0100

    Genauere Erklärung, was in der Beitrittsanfrage stehen soll

commit 46bd58f74d11591a0180319285b0c79b2212ef69 (tag: v4.1.8, original/stable-4.1)
Author: Claire <claire.github-309c@sitedethib.com>
Date:   Tue Sep 19 12:11:33 2023 +0200

    Bump version to v4.1.8
```

Es kommen erst unsere lokalen Patches und dann folgt irgendwann etwas, dass mit `(tag: v4.1.8)`
gekennzeichnet ist.

Das Update läuft dann wie folgt ab. Mit unserer Version machen wir

```
git rebase -i <VERSION>
```

`<VERSION>` ist dabei durch die Version zu ersetzen auf die wir aktualisieren wollen, z.B. `v4.1.9`.

Dabei kann es zu einem Merge-Fehler kommen (hoffentlich aber nicht). Der muss dann wie bei git
üblich gefixt werden (siehe Ausgabe von `git status`).

Jetzt müssen wir den entfernten Stand aktualisieren mit

```
git push --force
```

`--force` heißt, dass das auch erfolgreich ist, wenn bei gitlab Commits sind, die im aktuellen Stand
nicht enthalten sind. Das ist wegen dem `rebase` aber erwartet.

Jetzt geht es im Grunde genommen weiter wie im der Anleitung von Mastodon selbst.
Man muss allerdings das `git fetch && git checkout v4.1.9` ersetzen durch:
```
git status # Sicher gehen, dass es keine lokalen Änderungen gibt
git stash # nur, wenn es lokale Änderungen gibt

git fetch
git reset --hard origin/prod

git stash pop # nur, wenn man oben git stash gemacht hat
```

Achtung: `reset --hard` ist gefährlich. Vorher mit `git status` schauen, dass es keine lokalen
Änderungen gibt.

### Nach dem Update

Checkliste, was man nach einem Update, z.B. Host-System Major-Update oder Mastodon-Update oder Ruby-Update

* Mastodon-Web-Interface läuft noch
* Cronjobs in `crontab -l` von User mastodon funktionieren noch.
* Platte hat noch genug Platz.
* Übersetzung im Mastodon-Web-Interface funktioniert (Englisch nach Deutsch)

## lxd - Konfiguration

Damit neuere systemd Sandboxing-Optionen in lxd - Containern funktionieren, muss auf dem Host die Nesting-Funktion aktiviert werden:
```
lxc config set mastodon security.nesting true
```

## NGINX-Konfiguration

Mastodon war anfänglich nur mit einem NGINX auf dem Host konfiguriert. Bastian
hat diese Konfiguration am 29.08.2022 entfernt. Jetzt läuft im Mastodon-Container
eine NGINX mit einer Konfiguration ganz nahe an der empfohlenen Konfiguration,
außer dass ein selbstsigniertes Zertifikat benutzt wird (statt letsencrypt).
Außen auf dem Host läuft ein weiter NGINX mit einem simplen Forwarding an den Container.

```
server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name freiburg.social;

    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
    ssl_certificate /etc/letsencrypt/live/freiburg.social/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/freiburg.social/privkey.pem; # managed by Certbot

    location / {
        client_max_body_size 0;
        proxy_buffering off;
        proxy_cache off;
        proxy_read_timeout 1d;
        proxy_pass https://10.126.61.68;
        proxy_ssl_verify off;
        proxy_set_header Host $http_host;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location /.well-known/matrix {
        proxy_pass https://matrix.freiburg.social/.well-known/matrix;
    }
}
```

Das Zertifikat im Container wird wegen `proxy_ssl_verify off;` (default) nicht überprüft.

Die Section `location /.well-known/matrix` sorgt dafür, dass immer die aktuellen well-known-Dateien
von `matrix.freiburg.social` ausgeliefert werden.

In der `nginx.conf` wurde aufgrund von Meldungen im error-Log die Anzahl der worker_connections angehoben:
```
# fuer erhoehte worker_connections
worker_rlimit_nofile 10000;

events {
        # galaxis 20231024 Anzahl der worker_connections angehoben
        # worker_connections 768;
        worker_connections 4096;
```

## E-Mail versenden

```
$ grep -r SMTP_ /home/mastodon/live/.env.production
SMTP_SERVER=mail.freiburg.social
SMTP_PORT=25
SMTP_AUTH_METHOD=plain
SMTP_OPENSSL_VERIFY_MODE=none
SMTP_FROM_ADDRESS='freiburg.social Mastodon <mastodon@freiburg.social>'
SMTP_LOGIN=mastodon
SMTP_PASSWORD=XXX
```

Passwort fügt Bastian noch in einen Password-Manager ein.

Host wird erreicht über die interne IP mittels Eintrag in `/etc/hosts`:
```
10.126.61.16 mail.freiburg.social
```

## LibreTranslate

LibreTranslate ist nach [dieser Anleitung](https://sleeplessbeastie.eu/2022/11/17/how-to-use-libretranslate-with-mastodon/)
aufgesetzt.
Es gibt einen User `libretranslate` mit dem Home-Directory `/opt/libretranslate`
Dort ist als User mit `pip` LibreTranslate installiert:

```
pip3 install --user --prefer-binary libretranslate
```

Mit argospm kann man dann die Sprachdaten verwalten:
```
/opt/libretranslate/.local/bin/argospm -h                      # help
/opt/libretranslate/.local/bin/argospm update                  # update package-index
/opt/libretranslate/.local/bin/argospm search                  # list available language packs
/opt/libretranslate/.local/bin/argospm install translate-de_en # install language pack
/opt/libretranslate/.local/bin/argospm list                    # list installed language packs
```

Es gibt in `/etc/systemd/system/libretranslate.service` das SystemD unit file dafür:

```
[Unit]
Description=LibreTranslate
After=network.target

[Service]
User=libretranslate
Group=libretranslate
WorkingDirectory=/opt/libretranslate/
ExecStart=/opt/libretranslate/.local/bin/libretranslate --host 127.0.0.1 --port 5000 --disable-files-translation
Restart=always
#CPUQuota=50%

[Install]
WantedBy=multi-user.target
```

Man kann mit curl testen, ob es funktioniert (nachdem man es gestartet hat mit `systemctl enable --now libretranslate`

```
$ curl --request POST \
       --header "Content-type: application/json" \
       --data '{ "q": "Ich bin ein Hase", "source": "de", "target": "en"}'  \
       http://localhost:5000/translate
```

Dann ist die `/home/mastodon/live/.env.production` erweitert um:

```
# translate
ALLOWED_PRIVATE_ADDRESSES=127.0.0.1
LIBRE_TRANSLATE_ENDPOINT=http://127.0.0.1:5000
```

Danach muss man `mastodon-web` neustarten.


## Regelmäßige Jobs per systemd timer

Die Konfiguration der systemd timer liegt in `/etc/systemd/system`.
Es gibt dort zwei Jobs

```
# cat mastodon-media-remove.service
[Unit]
Description=Mastodon Media Remove
Requires=

[Service]
Type=oneshot
ExecStart=/home/mastodon/live/bin/tootctl media remove --verbose
Environment="RAILS_ENV=production" "LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libjemalloc.so.2" "PATH=/home/mastodon/.rbenv/shims:/home/mastodon/.rbenv/bin:/usr/local/bin:/usr/bin:/bin"
WorkingDirectory=/home/mastodon/live
StandardOutput=journal
User=mastodon
```

```
# cat mastodon-preview-cards-remove.service
[Unit]
Description=Mastodon Preview Cards Remove
Requires=

[Service]
Type=oneshot
ExecStart=/home/mastodon/live/bin/tootctl preview_cards remove --verbose
Environment="RAILS_ENV=production" "LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libjemalloc.so.2" "PATH=/home/mastodon/.rbenv/shims:/home/mastodon/.rbenv/bin:/usr/local/bin:/usr/bin:/bin"
WorkingDirectory=/home/mastodon/live
StandardOutput=journal
User=mastodon
```

Diese kann man manuell laufen lassen mit `systemctl start mastodon-media-remove.service` bzw.
`systemctl start mastodon-preview-cards-remove.service`.

Die Logausgaben gehen nach Journald. Wenn man sich für die Logzeilen interessiert,
die die jeweiligen Dienste ausgeben, kann man dieses filtern mit `journalctl --unit mastodon-media-remove.service`.

Der eigentliche Timer haben die Endung `.timer` und den gleichen Basisnamen. Dadurch „findet“ der
Timer auch den entsprechenden Service.

```
# cat mastodon-media-remove.timer
[Unit]
Description=Mastodon Media Remove Timer

[Timer]
OnBootSec=1h
OnUnitActiveSec=7d

[Install]
WantedBy=timers.target
```

`mastodon-preview-cards-remove.timer` ist äquivalent.

Beide Timer wurden aktiviert
```
systemctl enable --now mastodon-media-remove.timer
systemctl enable --now mastodon-preview-cards-remove.timer
```

Wenn man sich dafür interessiert, welche Timer wann zuletzt und wann das nächste Mal ausgeführt
werden, hilft

```
systemctl list-timers
```

## Fehler nach Update auf Debian 11 bullseye

Es gab einen Segmentation Fault nach dem Update auf Debian "bullseye".
Hinweis auf die Lösung gab ein Hinweis in einem [Kommentar auf Github](https://github.com/mastodon/mastodon/issues/15751).

Ich habe `/etc/systemd/system/mastodon-web.service` und `/etc/systemd/system/mastodon-sidekiq.service` erweitert um:

```
[Service]
Environment="LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libjemalloc.so.2"
```

Ähnliches findet sich jetzt auch am Ende von `/home/mastodon/.bashrc`:

```
export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libjemalloc.so.2
```

Das gleiche gilt auch für die service-Files für die regelmäßigen maintenance worker

## Setzen des Repository-Links

Klickt man auf den Link „Quellcode anzeigen“ im Mastodon Web Interface, kommt man auf unser
[Gitlab repository](https://gitlab.com/freiburg.social/mastodon).
Dies wird über folgende Zeite in `/etc/systemd/system/mastodon-web.service` erreicht:

```
Environment="SOURCE_BASE_URL=https://gitlab.com/freiburg.social/mastodon"
```


## Anpassungen an Service-Configs

### Elasticsearch
`/etc/elasticsearch/jvm.options.d/heap.options`
```
-Xms256m
-Xmx256m
```

### Postgres
Achtung: Muss nach Upgrade auf neue Postgres-Version (14 etc.) in neues Verzeichnis kopiert werden
Siehe https://pgtune.leopard.in.ua
`/etc/postgresql/13/main/conf.d/pgtune.conf`
```
# DB Version: 13
# OS Type: linux
# DB Type: web
# Total Memory (RAM): 3 GB
# CPUs num: 2
# Data Storage: hdd

max_connections = 200
shared_buffers = 768MB
effective_cache_size = 2304MB
maintenance_work_mem = 192MB
checkpoint_completion_target = 0.9
wal_buffers = 16MB
default_statistics_target = 100
random_page_cost = 4
effective_io_concurrency = 2
work_mem = 3932kB
min_wal_size = 1GB
max_wal_size = 4GB
max_worker_processes = 2
max_parallel_workers_per_gather = 1
max_parallel_workers = 2
max_parallel_maintenance_workers = 1
```

### Mastodon-sidekiq
Siehe auch https://gitlab.com/freiburg.social/freiburg.social/-/blob/master/docs/kandel/host.md#mastodon-settings

#### File Deskriptor Limit erhöht.
In `/etc/systemd/system/mastodon-sidekiq.service`:

```
[Service]
…
LimitNOFILE=500000
…
```

Ursache dafür war, dass wir in das Limit von 1024 gelaufen sind.

#### angepasste Sidekiq-Config
Sidekiq ist auf fünf Units verteilt, in denen die Queues mit unterschiedlichen Gewichtungen ausgeführt werden.
Siehe https://docs.joinmastodon.org/admin/scaling/#sidekiq

`mastodon-sidekiq.service` priorisiert externe Kommunikation (push,pull)
```
Environment="DB_POOL=15"
ExecStart=/home/mastodon/.rbenv/shims/bundle exec sidekiq -c 15 -q push,6 -q pull,1 -q mailers,1 -q default,1
```
`mastodon-sidekiq-ingress-[12].service` führen nur die Ingress-Queue aus, nachdem es im November '24 Probleme bei der Verarbeitung gab. Beide sind identisch konfiguriert:
```
Environment="DB_POOL=6"
ExecStart=/home/mastodon/.rbenv/shims/bundle exec sidekiq -c 6 -q ingress
```
`mastodon-sidekiq-local.service` priorisiert lokale Kommunikation (default) und führt den Scheduler aus
```
Environment="DB_POOL=20"
ExecStart=/home/mastodon/.rbenv/shims/bundle exec sidekiq -c 20 -q default,10 -q push,2 -q pull,2 -q scheduler,1 -q mailers,1
```
`mastodon-sidekiq-default.service` führt nur die default - Queue aus ("All tasks that affect local users")
```
Environment="DB_POOL=5"
ExecStart=/home/mastodon/.rbenv/shims/bundle exec sidekiq -c 5 -q default
```

Anzahl der Threads und Gewichtung müssen ggf. noch optimiert werden

### verringertes Logging
Da journald mit dem Rotieren der Journals nicht mehr hinterherkam, ist der Standard-Loglevel für Rails auf "warn" reduziert:
`~mastodon/live/.env.production`
```
# reduce logging (default "info")
RAILS_LOG_LEVEL=warn
```

### Elasticsearch

https://docs.joinmastodon.org/admin/optional/elasticsearch/

...allerdings mit Elasticsearch-oss, ohne die proprietären Komponenten.

```
# apt-key ist deprecated
curl https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
echo "deb https://artifacts.elastic.co/packages/oss-7.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-7.x-oss.list

apt update
# Elasticsearch kommt mit einer eigenen jre
# apt install default-jre openjdk-11-jre-headless
apt install elasticsearch-oss
```
In `/etc/elasticsearch/jvm.options.d/heap.options`:
```
-Xms1024m
-Xmx1024m
```

```
systemctl enable elasticsearch
systemctl start elasticsearch
```

In `~mastodon/live/.env.production`:
```
ES_ENABLED=true
ES_HOST=localhost
ES_PORT=9200
```

```
su - mastodon
cd live
RAILS_ENV=production bin/tootctl search deploy
exit

systemctl restart mastodon-*
```

### Ramdisk für /tmp
Da die Verarbeitung eingehender Mediendaten ständig temporäre Dateien unter /tmp erzeugt und löscht, wurde das Verzeichnis auf eine Ramdisk verlegt. 
```
cp /usr/share/systemd/tmp.mount /etc/systemd/system/tmp.mount
systemctl enable tmp.mount
reboot
```
