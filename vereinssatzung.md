[_metadata_:title]:- "freiburg.social - Vereinssatzung"
[_metadata_:author]:- "der Verein ;)"
[_metadata_:version]:- "2"
[_metadata_:date]:- "2021-07-21"
[_metadata_:tags]:- "verein satzung vereinssatzung formelles"

```
  __          _ _                                      _       _
 / _|        (_) |                                    (_)     | |
| |_ _ __ ___ _| |__  _   _ _ __ __ _   ___  ___   ___ _  __ _| |
|  _| '__/ _ \ | '_ \| | | | '__/ _` | / __|/ _ \ / __| |/ _` | |
| | | | |  __/ | |_) | |_| | | | (_| |_\__ \ (_) | (__| | (_| | |
|_| |_|  \___|_|_.__/ \__,_|_|  \__, (_)___/\___/ \___|_|\__,_|_|
                                 __/ |
                                |___/
```
# Vereinssatzung

## Präambel

1.  An Stellen dieser Satzung, an denen schriftliche Kommunikation gefordert
    wird, sind E-Mail sowie weitere geeignete digitale Mittel stets mit
    eingeschlossen.
2.  An Stellen dieser Satzung, an denen eine Beschlussfassung im Konsens
    gefordert wird, ist eine Zustimmung aller anwesenden, stimmberechtigten
    Personen im Sinne von Abwesenheit eines Widerspruchs gemeint. Ein Konsens
    ist damit einer einstimmigen Entscheidung gleichzusetzen.
3.  An Stellen dieser Satzung, an denen von anwesenden Personen die Sprache
    ist, ist eine virtuelle Anwesenheit mittels digitaler Lösungen mitgemeint.

# § 1 Name, Sitz, Geschäftsjahr

1.  Der Verein führt den Namen "freiburg.social".
2.  Er soll in das Vereinsregister eingetragen werden. Nach der Eintragung
    führt er zu seinem Namen den Zusatz e. V.
3.  Der Verein hat seinen Sitz in Freiburg im Breisgau.
4.  Das Geschäftsjahr ist das Kalenderjahr.

# § 2 Zweck des Vereins

1.  Die Zwecke des Vereins sind:
    *   Die Förderung von vertrauenswürdigen, datensparsamen und sicheren
        digitalen Diensten auf Basis von freier Software.
    *   Die Aufklärung und Bildung zu den Themen:
        *   freie Software
        *   sichere digitale Kommunikation
        *   Datensparsamkeit
    *   Die Förderung von Diskussion, Austausch und Vernetzung über digitale
        Medien in der Region Freiburg.
    *   Die Bereitstellung von vertrauenswürdigen, datensparsamen und sicheren
        digitalen Diensten auf Basis von freier Software.
2.  Der Satzungszweck wird insbesondere verwirklicht durch das Anbieten von
    freien und offenen digitalen Diensten, mit Fokus auf den Großraum Freiburg
    im Breisgau.
3.  Die Mitglieder der Organe des Vereins, sowie mit Aufgaben zur Förderung des
    Vereins betraute Mitglieder, haben gegenüber dem Verein einen Anspruch auf
    Ersatz der ihnen in Zusammenhang mit ihrer Amtsausübung entstandenen
    Aufwendungen (§ 470 BGB) im Rahmen der Beschlüsse des Vorstandes und im
    Rahmen der finanziellen Leistungsfähigkeit des Vereins.
    Aufwandsentschädigungen für geleistete Arbeit im Rahmen der
    Vorstandstätigkeit sind auch an Mitglieder des Vorstandes möglich,
    sofern die Mitgliederversammlung entsprechendes beschließt.

# § 3 Mitgliedschaft

1.  Die Mitgliedschaft kann von jeder natürlichen Person erworben werden,
    die sich zum Vereinszweck bekennt.
2.  Die Mitgliedschaft muss gegenüber dem Vorstand schriftlich beantragt werden.
    Über den schriftlichen Aufnahmeantrag entscheidet der Vorstand.
    Will der Vorstand einen Aufnahmeantrag ablehnen,
    so legt er ihn der nächsten ordentlichen Mitgliederversammlung vor.
    Diese entscheidet endgültig.
    Der Verein ist gegenüber der antragsstellenden Person nicht verpflichtet,
    Ablehnungsgründe mitzuteilen.
3.  Die Mitgliedschaft beginnt mit der Aushändigung einer schriftlichen
    Bestätigung durch ein Vorstandsmitglied.
4.  Die Mitgliedschaft endet
    *   durch freiwilligen Austritt;
    *   durch Ausschluss aus dem Verein gemäß § 4;
    *   mit dem Tod des Mitglieds.
5.  Der Austritt ist gegenüber dem Vorstand mit einer zweiwöchigen Frist
    schriftlich zu erklären.
6.  Bei Beendigung der Mitgliedschaft, gleich aus welchem Grund,
    erlöschen alle Ansprüche aus dem Mitgliedsverhältnis.
    Eine Rückgewähr von Spenden oder sonstigen Unterstützungsleistungen
    ist grundsätzlich ausgeschlossen.
7.  Dem Mitglied ist beim Eintritt in den Verein eine schriftliche oder
    digitale Kopie dieser Satzung auszuhändigen.

# § 4 Ausschluss aus dem Verein

1.  Der Ausschluss eines Mitgliedes mit sofortiger Wirkung und aus wichtigem
    Grund kann dann ausgesprochen werden,
    wenn das Mitglied in grober Weise dem Zwecke, der Satzung, den Zielen oder
    der Ordnung des Vereins zuwider handelt oder das Ansehen des Vereins in der
    Öffentlichkeit in grober Weise schädigt.
2.  Über den Ausschluss entscheidet der Vorstand.
3.  Dem Mitglied ist unter Fristsetzung von zwei Wochen Gelegenheit zu geben,
    sich zu den Vorwürfen zu äußern.
    Legt das Mitglied gegen den Ausschluss Widerspruch beim Vorstand ein,
    so entscheidet die Mitgliederversammlung endgültig über den Ausschluss.

# § 5 Rechte und Pflichten der Mitglieder

1.  Die Mitglieder sind berechtigt, an allen angebotenen Veranstaltungen des
    Vereins teilzunehmen.
    Sie haben darüber hinaus das Recht,
    gegenüber dem Vorstand und der Mitgliederversammlung,
    Anträge zu stellen und an Abstimmungen teilzunehmen.
2.  Die Mitglieder sind verpflichtet, den Verein und Vereinszweck -
    auch in der Öffentlichkeit - in ordnungsgemäßer Weise zu unterstützen.
3.  Der Verein erhebt keinen Mitgliedsbeitrag.

# § 6 Organe des Vereins

1.  Die Organe des Vereins sind der Vorstand und die Mitgliederversammlung.
2.  Es können nur Mitglieder einem Organ des Vereins angehören.

# § 7 Vorstand

1.  Der Vorstand im Sinne des § 26 BGB besteht aus dem ersten Vorsitz,
    dem zweiten Vorsitz sowie der Kassenführung.
    Es können spezielle Zuständigkeiten oder Aufgabengebiete festglegt werden.
    Ansonsten sind alle drei Mitglieder des Vorstands gleichberechtigt.
2.  Der Verein wird gerichtlich und außergerichtlich durch zwei Mitglieder des
    Vorstandes vertreten.
    Sie sind hierbei an die Beschlüsse des Vorstands gebunden.

# § 8 Zuständigkeiten des Vorstandes

Der Vorstand ist für alle Angelegenheiten des Vereins zuständig,
soweit sie nicht durch die Satzung oder zusätzliche Vereinsordnungen nach
§ 13 einem anderen Vereinsorgan zugewiesen sind, oder der Vorstand beschließt,
sie an eine Arbeitsgruppe zu delegieren.

# § 9 Amtsdauer des Vorstandes

1.  Der Vorstand wird von der Mitgliederversammlung für die Dauer von einem Jahr
    gewählt. Die unbegrenzte Wiederwahl ist möglich.
    Der Vorstand bleibt bis zum Amtsantritt seiner Nachfolge im Amt.
2.  Jedes Vorstandsmitglied ist einzeln zu wählen.
    Wählbar sind nur Vereinsmitglieder, nach § 3.
3.  Das Amt eines Mitglieds des Vorstands endet durch schriftliche Mitteilung
    des Rücktritts oder mit dem Ausscheiden des Mitglieds aus dem Verein.
    Endet das Vorstandsamt eines Mitglieds aus einem dieser Gründe vor Ablauf
    der Amtsdauer, bestimmen die verbleibenden Mitglieder des Vorstands ein
    Ersatzmitglied für den Rest der Amtszeit des ausgeschiedenen Mitglieds.

# § 10 Beschlussfassung des Vorstandes

1.  Der Vorstand fasst seine Beschlüsse in Vorstandssitzungen.
    Diese werden durch den ersten Vorsitz,
    bei dessen Verhinderung durch den zweiten Vorsitz,
    schriftlich und vereinsöffentlich mit einer Frist von 7 Tagen einberufen.
    Der Mitteilung einer Tagesordnung bedarf es nicht.
2.  Der Vorstand ist bei Anwesenheit von mindestens zwei Mitgliedern des
    Vorstands beschlussfähig und entscheidet im Konsens.
3.  Der erste und zweite Vorsitz sind von den Beschränkungen
    des § 161 BGB befreit.
4.  Die Sitzung ist zu protokollieren.
    Der Vorstand bestimmt zu Beginn jeder Sitzung eine schriftführende Person,
    die das Protokoll anfertigt.
5.  Das Protokoll jeder Sitzung ist innerhalb von 7 Tagen nach der Sitzung
    vereinsöffentlich schriftlich bekanntzumachen.
6.  Die Vorstandssitzungen sind grundsätzlich vereinsöffentlich.
7.  Für Angelegenheiten, die das Diskutieren personenbezogener Daten erfordern,
    kann der Vorstand über den Ausschluss der Öffentlichkeit oder von Teilen der
    Öffentlichkeit entscheiden.
8.  Auf den Vorstandssitzungen haben die Vorstandsmitglieder Stimm- und
    Rederecht. Ansprechpersonen der Arbeitsgruppen nach § 11 Abs. 14 haben
    Rederecht in Angelegenheiten, die ihren jeweiligen Zweck betreffen.
    Der Vorstand kann für jede sonstige anwesende Person über das Stimmrecht
    entscheiden.
9.  Bei wiederholter Störung der Sitzung kann der Vorstand beschließen,
    betreffenden Anwesenden von der weiteren Teilnahme an der Sitzung
    auszuschließen.
10. Vorstandsmitglieder und andere Personen können zu jeder Sitzung mit
    geeigneten digitalen Hilfsmitteln, wie Video-Konferenzlösungen,
    zugeschaltet werden. So zugeschaltete Mitglieder können ihr volles
    Stimmrecht wahrnehmen und gelten als anwesend.
    Es ist ebenfalls möglich, rein virtuelle Sitzungen unter Zuhilfenahme
    digitaler Hilfsmittel abzuhalten.

# § 11 Mitgliederversammlung

1.  Die Mitgliederversammlung ist das oberste Beschlussorgan des Vereins.
    Ihr obliegen alle Entscheidungen, die nicht vom Vorstand getroffen werden
    dürfen, sowie Entscheidungen, die den Vorstand oder seine Mitglieder direkt
    betreffen.
2.  Die Beschlüsse der Mitgliederversammlung werden im Konsens gefasst.
3.  Die Mitgliederversammlung ist grundsätzlich ein Mal pro Geschäftsjahr
    einzuberufen.
4.  In der Regel wird die Mitgliederversammlung durch den Vorstand einberufen.
5.  Die Einladung zur Mitgliederversammlung muss spätestens 14 Tage vorher
    schriftlich durch den Vorstand an alle Mitglieder nach § 3 versandt werden.
6.  Die Mitgliederversammlung wird durch den Vorstand geleitet.
7.  Die Mitgliederversammlung entlastet den Vorstand.
8.  Die Mitgliederversammlung kann Änderungen an der Vereinssatzung
    mit 2/3-Mehrheit vornehmen.
9.  Die Mitgliederversammlung ist unabhängig von der Anzahl anwesender
    Mitglieder nach § 3 beschlussfähig. Die Anwesenheit kann durch den Einsatz
    von geeigneten digitalen Hilfsmitteln, wie Video-Konferenzlösungen,
    sichergestellt werden. Es ist ebenfalls möglich, rein virtuelle
    Mitgliederversammlungen unter Zuhilfenahme digitaler Hilfsmittel abzuhalten.
10. Anträge an die Tagesordnung sind spätestens 3 Tage vor der
    Mitgliederversammlung an den Vorstand zu stellen.
11. Die Mitgliederversammlung kann zusätzlich einmal pro Quartal auf Verlangen
    von 1/3 der Mitglieder nach § 3 einberufen werden.
    Dies kann nur durch die Angabe eines triftigen Grundes erfolgen.
    Das Verlangen ist dem Vorstand schriftlich mitzuteilen.
    In diesem Fall muss die Mitgliederversammlung spätestens 21 Tage nach der
    Mitteilung stattfinden.
12. Die Mitgliederversammlung kann die Abberufung des Vorstandes fordern, in
    diesem Fall ist eine 2/3-Mehrheit erforderlich. Die Wahl eines neuen
    Vorstandes erfolgt auf derselben Mitgliederversammlung nach § 9 Abs. 1f.
13. Wahlen finden grundsätzlich offen statt, auf Antrag einer wahlberechtigten
    Person geheim.
14. Die Mitgliederversammlung kann zweckgebundene Arbeitsgruppen bilden.
    Jede Arbeitsgruppe wird durch eine Ansprechperson geleitet,
    die durch die Mitgliederversammlung bestimmt wird.
    Arbeitsgruppen bilden kein eigenes Vereinsorgan.
15. Der Verlauf und die Beschlüsse der Mitgliederversammlung sind zu
    protokollieren. Die Mitgliederversammlung bestimmt zu Beginn jeder Sitzung
    eine schriftführende Person, die das Protokoll anfertigt.
16. Das Protokoll jeder Mitgliederversammlung ist von der schriftführenden
    Person und einem Vorstandsmitglied zu unterzeichnen.
17. Das Protokoll jeder Mitgliederversammlung ist innerhalb von 14 Tagen nach
    der Sitzung vereinsöffentlich schriftlich bekanntzumachen.

# § 12 Kassenprüfung

1.  Die Mitgliederversammlung wählt für die Dauer von einem Jahr mindestens
    zwei Mitglieder zur Kassenprüfung.
    Die Kassenprüfenden dürfen nicht dem Vorstand angehören.
2.  Die Kassenprüfenden sind berechtigt, die Kassenführung des Vorstandes
    laufend zu überwachen und dazu die entsprechenden Unterlagen einzusehen.
    Die Kassenprüfenden berichten darüber auf der jährlichen
    Mitgliederversammlung und entlasten die Kassenführung.

# § 13 Ergänzende Dokumente

1.  Der Verein kann sich zur Regelung der vereinsinternen Abläufe
    Vereinsordnungen geben.
    Die Vereinsordnungen sind nicht Bestandteil der Satzung.
2.  Über den Erlass, die Änderung und Aufhebung von Vereinsordnungen entscheidet
    die Mitgliederversammlung.

# § 14 Mitgliedschaft in anderen Vereinen

1.  Der Verein darf Mitglied in anderen Vereinen werden.
2.  Die Mitgliederversammlung entscheidet über den Beitritt bzw. Austritt aus
    anderen Vereinen.

# § 15 Salvatorische Klausel

Die Mitgliederversammlung ermächtigt den Vorstand Satzungsänderungen
selbstständig vorzunehmen, die aufgrund von Moniten des zuständigen
Registergerichts oder des Finanzamtes notwendig werden und die den Kerngehalt
einer zuvor beschlossenen Satzungsänderung nicht berühren.
Der Vorstand hat die textliche Änderung im Konsens zu beschließen.
In der auf den Beschluss folgenden Mitgliederversammlung ist diese von der
Satzungsänderung in Kenntnis zu setzen.

# § 16 Auflösung des Vereins

1.  Die Auflösung des Vereins muss von der Mitgliederversammlung mit
    einer 2/3-Mehrheit beschlossen werden.
2.  Die Abstimmung ist nur möglich, wenn auf der Einladung zur
    Mitgliederversammlung als einziger Tagesordnungspunkt die Auflösung des
    Vereins angekündigt wurde.
3.  Bei Auflösung des Vereins fällt das Vereinsvermögen an den Free Software
    Foundation Europe e.V., Hamburg, der es ausschließlich und unmittelbar zu
    steuerbegünstigten Zwecken zu verwenden hat.
